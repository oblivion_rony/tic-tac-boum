import pygame
import Son
import Duel
import SixADeux
import settings
from pygame.locals import *

pygame.init()

BLACK = (0,0,0)
WHITE = (255,255,255)   	
color = (255,255,255)

bg_color = (255,0,0)

#Le nom affiché en haut de la fenêtre.
pygame.display.set_caption("Tic Tac Boum !") 
#Definition de la taille de l'ecran ainsi que son mode de fonctionnement
screen = pygame.display.set_mode((1200, 600), RESIZABLE)

#Variable qui continue la boucle si = 1, stoppe si = 0
continuer = True
#appel de la class SixADeux pour initialiser les variables (name,players,jingles,tabsons,lifes)

#Chargement des Class via les fonctions __init__

#Parametres: les dossiers des Jingles et Son 
#ou des valeurs par default pour les dossiers
son = Son.Son()
#Parametres: l'ecran, la couleur de fond, le font, la taille d'écriture et le fichier de sauvegarde
#ou valeurs par défault pour le font, la taille d'écriture et le fichier de sauvegarde
settings = settings.Settings(screen, color)
#Duel et SixADeux 
#Parametres: l'ecran, la couleur de fond, l'instance setting, un tableau des noms de joueur et le nombre de vie des joueurs
#ou valeurs par défault un tableau des noms de joueur et le nombre de vie des joueurs
duel = Duel.Duel(screen, color, bg_color, settings)
sixADeux = SixADeux.SixADeux(screen, color, settings)

val_partie = ''
#Boucle infinie
while continuer:
    #Gestion des évènements
	for event in pygame.event.get():
		#Si un de ces evenements est de type QUIT ou que l'on appui sur echap 
		if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE or val_partie == 'quit':		
			#On arrete la boucle
			continuer = False
		#Si un evenement est de type touche appuyer
		elif event.type == VIDEORESIZE:
			screen = pygame.display.set_mode((event.w, event.h), RESIZABLE)
		elif event.type == KEYDOWN:
			#Si on appuie sur la touche pour le Menu Duel
			#Touche definie dans le fichier touches.txt et mis sous tableau dans settings
			if event.key == settings.touches[0]:
				#Valeur de retour pour informer que l'on quitte un des menus 
				#Permet de lancer la fonction qui va s'occuper de la partie duel
				val_partie = duel.start()
			#Si on appuie sur la touche pour le Menu 6 a 2
			#Touche definie dans le fichier touches.txt et mis sous tableau dans settings
			elif event.key == settings.touches[1]:
				#Valeur de retour pour informer que l'on quitte un des menus 
				#Permet de lancer la fonction qui va s'occuper de la partie 6 a 2
				val_partie = sixADeux.start()
			#Si on appuie sur la touche pour les Jingles
			#Touche definie dans le fichier touches.txt et mis sous tableau dans settings
			elif event.key == settings.touches[3]:
				#On vient lancer le jingle TicTacBoum
				#Parametre: donner la position du jingle via le nom du fichier
				son.playVideo(son.jingles_name.index("tictacboum"))
			#Si on appuie sur la touche pour le Menu Setting
			#Touche definie dans le fichier touches.txt et mis sous tableau dans settings
			elif event.key == settings.touches[2]:
				#Valeur de retour pour informer que l'on quitte un des menus 
				#Permet de lancer la fonction qui va s'occuper du fichier settings
				val_partie = settings.main()
	#Remplissage d'une couleur de fond
	#Parametre: couleur (r,g,b)
	screen.fill(color) 
	#Affichage des modifications
	pygame.display.flip()

pygame.quit()
