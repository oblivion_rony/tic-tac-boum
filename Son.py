from moviepy.editor import *
import pygame
from pygame.locals import *
import glob

class Son(object):
	#Parametres : "dossier_bruitages dossier_jingles" les dossier ou sont enregistrer les bruitages et jingles du jeu
	def __init__(self, dossier_bruitages = "sfx", dossier_jingles = "Jingles"):
		#glob vient lire les elements dans un dossier 
		self.bruitages_files = glob.glob("{}/*.ogg".format(dossier_bruitages))
		self.jingles_files = glob.glob("{}/*.mp4".format(dossier_jingles))
		self.bruitagesSound = []
		self.jinglesVideo = []
		self.bruitages_name = []
		self.jingles_name = []
		#Ajout des sons dans le tableau des bruitages
		for bruitage in self.bruitages_files:
			self.bruitagesSound.append(pygame.mixer.Sound(bruitage))
		#Ajout des sons dans le tableau des jingles
		for jingle in self.jingles_files:
			self.jinglesVideo.append(VideoFileClip(jingle))
		for bruitage_file in self.bruitages_files:
			#Recupere la position du premier slash
			slash = bruitage_file.find("\\")
			#Recupere la position du premier point en partant de la fin
			point = bruitage_file.rfind(".")
			#Ajout le nom du fichier des bruitages dans le tableau de nom
			self.bruitages_name.append(bruitage_file[slash+1:point])
		for jingle_file in self.jingles_files:
			#Recupere la position du premier slash
			slash = jingle_file.find("\\")
			#Recupere la position du premier point en partant de la fin
			point = jingle_file.rfind(".")
			#Ajout le nom du fichier des jingles dans le tableau de nom
			self.jingles_name.append(jingle_file[slash+1:point])

	#Joue le nieme bruitage du tableau
	#Parametre : "i" position du son dans le tableau des bruitages
	def playSon(self, i):
		self.bruitagesSound[i].play()
	#Mets le nieme bruitage du tableau en pause
	#Parametre : "i" position du son dans le tableau des bruitages
	def pauseSon(self,i):
		self.bruitagesSound[i].stop()

	#Joue le nieme bruitage du tableau
	#Parametre : "i" position du jingle dans son tableau 
	def playVideo(self,i):
		self.jinglesVideo[i].preview()
	
	#Fonction qui recupere le volume des bruitages
	def getVolume(self):
		return self.bruitagesSound[0].get_volume()

	#Fonction qui augmente le volume des bruitages
	def volumePP(self):
		self.bruitagesSound[0].set_volume(self.getVolume()+0.1)

	#Fonction qui diminue le volume des bruitages
	def volumeM(self):
		self.bruitagesSound[0].set_volume(self.getVolume()-0.1)