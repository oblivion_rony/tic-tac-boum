import game
import Son
import AnimationBombe
import pygame
import pupitre
import ListingPhoneme
    
from pygame.locals import *

class Duel():

    def __init__(self, screen, color, bg_color, settings, player_name = ['J1','J2'], nb_life = 2):
        self.continuer = False
        #appel de la class Duel pour initialiser les variables (name,players,jingles,tabsons,lifes)
        self.duel = game.Duel('duel',player_name,nb_life)
        self.screen = screen
        self.animbombe = AnimationBombe.AnimationBombe(self.screen)
        self.bg_color = bg_color
        self.color = color
        #Chargement image
        vie = "Images/Vie.png"
        self.vie = pygame.image.load(vie).convert_alpha()
        self.pupitre = pupitre.Pupitre()
        self.listingPhoneme = ListingPhoneme.Phoneme(screen, "Images/Phonemes/miniatures")
        self.son = Son.Son()
        self.settings = settings

    def lose(self, player):
        self.son.pauseSon(self.son.bruitages_name.index("tictac"))
        self.son.playSon(self.son.bruitages_name.index("explosion"))
        val = self.duel.wrong(player)
        #On appelle la methode wrong avec en parametre 0 pour dire que le joueur 1 s'est trompe
        if val:
            self.son.playSon(self.son.bruitages_name.index("Tic_Tac_victoire"))
            self.duel.reset()
            self.pupitre.loseLumiere(0.1)
            self.continuer = False
        elif val == False:
            self.pupitre.loseLumiere(0.2)
            self.duel.next()
            self.animbombe.reset()
        self.duel.pause = not self.duel.pause
        #Tire un phoneme pour la premiere manche
        self.listingPhoneme.tirer_phoneme()

    def start(self):
        #self.son.playVideo(self.son.jingles_name.index("duel"))
        self.pupitre.GPIO_setup()
        self.continuer = True
        #Tire un phoneme pour la premiere manche
        self.listingPhoneme.tirer_phoneme()
        #Boucle infinie
        while self.continuer:
                # Gestion des évènements
            self.pupitre.pupitreJoueurDuel(self.duel.lifes, self.duel.num_joueur_actif_total)
            if self.pupitre.actionChange(self.duel.num_joueur_actif_total):
                self.duel.next()
                self.son.playSon(self.son.bruitages_name.index("6a2_passe_bombe"))
                self.animbombe.inverse()

            for event in pygame.event.get():
                # Si un de ces événements est de type QUIT
                if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:		
                    # On arrête la boucle
                    self.continuer = False 
                    return 'quit'   
                elif event.type == KEYDOWN:
                    #Si on appuie sur la 1 du pad
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    if event.key == self.settings.touches[4]:
                        #On appelle la methode wrong avec en parametre 0 pour dire que le joueur 1 s'est trompe
                        self.lose(self.duel.num_joueur_actif_total-1)
                    #Si on appuie sur la 2 du pad
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[5]:
                        #On appelle la methode wrong avec en parametre 1 pour dire que le joueur 2 s'est trompe
                        self.lose(0)
                    #Si on appuie sur la fleche droite
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[6]:
                        #On appelle la methode wrong avec en parametre 1 pour dire que le joueur 2 s'est trompe
                        self.lose(1)
                    #Si on appuie sur la fleche droite
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[11]:
                        #On appelle la méthode next pour passer au joueur suivant
                        self.duel.next()
                        self.son.playSon(self.son.bruitages_name.index("6a2_passe_bombe"))
                        self.animbombe.inverse()
                    #Si on appuie sur le plus du pad
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[12]:
                        #On appelle la méthode back pour revenir en arriere sur le dernier coup (erreur)
                        self.duel.back(self.duel.position-1)
                    #Si on appuie sur la barre espace
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[13]:
                        #On appelle la méthode back pour revenir en arriere sur le dernier coup (erreur)
                        self.animbombe.setAngle(6)
                        self.duel.pause = not self.duel.pause
                        self.animbombe.pause = not self.animbombe.pause
                        if not self.duel.pause:
                            self.son.playSon(self.son.bruitages_name.index("tictac"))
                        else:
                            self.son.pauseSon(self.son.bruitages_name.index("tictac"))
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[14]:
                        self.son.volumeM()
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[15]:
                        self.son.volumePP()
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[3]:
                        self.duel.pause = True
                        self.animbombe.pause = True
                        self.son.playVideo(self.son.jingles_name.index("duel"))

            if self.animbombe.run():
                self.lose(self.duel.num_joueur_actif_total)

            #changement du fond et de l'image
            self.screen.fill (self.color)
            if self.duel.num_joueur_actif > 0 :
                posx = self.screen.get_size()[0]/2
            else :
                posx = 0
            pygame.draw.rect(self.screen, self.bg_color, (posx,0,self.screen.get_size()[0]/2,self.screen.get_size()[1]))
            #changement du fond et de l'image
            self.screen.blit(self.animbombe.image, self.animbombe.image_rect)

            #Appel de la fonction pour afficher le phoneme
            self.listingPhoneme.afficherPhon()
            #Affichage Vies
            #On défini l'écart entre les vies puis on les lie au tableau de vie du joueur
            ecart = 200

            #Joueur1
            for i in range (self.duel.lifes[0]+1): 
                self.screen.blit(self.vie, (0,400-ecart*i)) # i est éagle au vies 0, 1, 2

            #Joueur2
            for i in range (self.duel.lifes[1]+1):
                self.screen.blit(self.vie, (self.screen.get_size()[0]*17/20,400-ecart*i)) # i est éagle au vies 0, 1, 2

            # Rafraîchissement de l'écran
            pygame.display.flip()
