import pygame
from math import *
from pygame.locals import *

class Settings(object):
	#Fonction __init__ 
	#Parametres : ecran, couleur de fond, font d'ecriture, taille d'ecriture, taille d'ecriture des titres, fichier de sauvegarde des touches
	def __init__(self, screen, color, font=None, size_font=36, size_font2=80, file='touches.txt'):
		self.screen = screen
		self.color = color
		#Creation de variable font avec la police d'ecriture ainsi que sa taille
		self.font = pygame.font.Font(None, size_font)
		self.font2 = pygame.font.Font(None, size_font2)

		#Permet de transformer un string avec une font, arrondir les lettres si a 1, ainsi qu'une couleur d'ecriture
		self.titre = self.font2.render("Setting", 1, (0,0,0))
		#Permet de transformer la surface du font en un rectangle
		self.titre_rect = self.titre.get_rect()
		#Permet de definir la position du titre en fonction de la taille de l'ecran
		self.titrex = self.screen.get_size()[0]/2
		self.titrey = self.screen.get_size()[1]*3/40
		#Permet de definir le centre du rectangle avec les positions
		self.titre_rect.center = (self.titrex, self.titrey)

		self.retour = self.font.render("Retour", 1, (0,0,0))
		self.retour_rect = self.retour.get_rect()
		self.retourx = self.screen.get_size()[0]*3/4
		self.retoury = self.screen.get_size()[1]*3/40
		self.retour_rect.center = (self.retourx, self.retoury)

		self.standby = self.font.render("|",1,(150,150,150))
		self.standby_rect = self.standby.get_rect()

		self.file = file

		#Ouverture du fichier (touches.txt) donner en parametre en mode read 'r' (lecture en mode texte)
		try:
			fichierR = open(self.file,'r')
		except IOError:
			fichierR = open(self.file,'w')      
			#Ecriture dans le fichier avec la methode write()
			fichierR.write("282\n283\n284\n106\n256\n257\n258\n259\n260\n261\n262\n271\n8\n32\n269\n270")

		#Lecture dans le fichier avec la methode read()
		chaine = fichierR.read()
		#Fermeture du fichier avec la methode close()
		fichierR.close()
		#Separation de la chaine en fonction du saut a la ligne dans une liste
		chaine = chaine.split("\n")

		#Creation d'un tableau 1D pour les differentes valeurs des touches
		self.touches = []

		#Transformation des elements de la liste en integer
		for i in range(len(chaine)):
			self.touches.append(int(chaine[i]))

		#Creation d'un tableau 2D avec l'intituler de la touche ainsi que sa valeur via le tableau 1D
		self.tabText = ["Menu Duel",pygame.key.name(self.touches[0]),
			"Menu 6 a 2",pygame.key.name(self.touches[1]),
			"Menu Settings",pygame.key.name(self.touches[2]),
			"Jingles",pygame.key.name(self.touches[3]),
			"Joueur précédent à éliminer",pygame.key.name(self.touches[4]),
			"Joueur 1 à éliminer",pygame.key.name(self.touches[5]),
			"Joueur 2 à éliminer",pygame.key.name(self.touches[6]),
			"Joueur 3 à éliminer",pygame.key.name(self.touches[7]),
			"Joueur 4 à éliminer",pygame.key.name(self.touches[8]),
			"Joueur 5 à éliminer",pygame.key.name(self.touches[9]),
			"Joueur 6 à éliminer",pygame.key.name(self.touches[10]),
			"Joueur suivant",pygame.key.name(self.touches[11]),
			"Retour en arrière",pygame.key.name(self.touches[12]),
			"Play / Pause",pygame.key.name(self.touches[13]),
			"Volume -",pygame.key.name(self.touches[14]),
			"Volume +",pygame.key.name(self.touches[15]),
		]

		#Création d'un tableau a 2 dimensions pour y ranger les différents surfaces (texte / valeurs touches)
		self.menu = []
		for _ in range(len(self.tabText)):
			self.menu.append([0] *4 )

		for i in range(len(self.tabText)):
			if i%2 == 0 or i==0:
				#Permet de transformer un string avec une font, arrondir les lettres si a 1, ainsi qu'une couleur d'ecriture
				self.menu[i][0] = self.font.render(self.tabText[i], 1, (0,0,0))
				#Permet de transformer la surface du font en un rectangle
				self.menu[i][1] = self.menu[i][0].get_rect()
				#Permet de definir la position du titre en fonction de la taille de l'ecran
				self.menu[i][2] = self.screen.get_size()[0]/3
				self.menu[i][3] = self.screen.get_size()[1]*(i+6)/40
				#Permet de definir le centre du rectangle avec les positions
				self.menu[i][1].topleft = (self.menu[i][2], self.menu[i][3])
			else:
				#Permet de transformer un string avec une font, arrondir les lettres si a 1, ainsi qu'une couleur d'ecriture
				self.menu[i][0] = self.font.render(self.tabText[i], 1, (150,150,150))
				#Permet de transformer la surface du font en un rectangle
				self.menu[i][1] = self.menu[i][0].get_rect()
				#Permet de definir la position du titre en fonction de la taille de l'ecran
				self.menu[i][2] = self.screen.get_size()[0]*2/3
				self.menu[i][3] = self.screen.get_size()[1]*(i+5)/40
				#Permet de definir le centre du rectangle avec les positions
				self.menu[i][1].topleft = (self.menu[i][2], self.menu[i][3])

	#Fonction qui permet d'afficher de rafraichir l'ecran avec son fond de base puis les differents menus
	#Parametre : "pos" qui défini l'endroit ou l'utilisateur a cliquer pour modifier
	def affichage(self, pos):
		self.menu[pos][1].topleft = (self.menu[pos][2], self.menu[pos][3])
		self.standby_rect.topleft = (self.menu[pos][2], self.menu[pos][3])
		self.screen.fill(self.color)
		#Affiche les 2 surfaces "Settings" et "Retour"
		self.screen.blit(self.titre, self.titre_rect)
		self.screen.blit(self.retour, self.retour_rect)
		#Boucle qui va permettre d'afficher les differents menus avec leurs touches
		for i in range(len(self.menu)):
			if i == pos:
				#Permet d'afficher la Surface attente "|" a l'endroit ou l'utilisateur a cliquer
				self.screen.blit(self.standby, self.standby_rect)
			else:
				#Permet d'afficher tous les autres menus non selectionner
				self.screen.blit(self.menu[i][0], self.menu[i][1])
		pygame.display.flip()

	#Fonction qui attend une touche presser et viens la modifier pour l'affichage et le tableau avec les differentes touches
	#Parametre : "pos" permet de donner la position de la touche modifier via le tableau 2D 
	def modifTouche(self, pos):
		#Permet de connaitre via la position "pos" d'un tableau 2D, la position d'un tableau 1D "pos_touches"
		#ceil arrondi superieur de la valeur donner
		pos_touches = ceil(pos/2)-1
		while 1:
			for event in pygame.event.get():
				if event.type == KEYDOWN:
					#Si la touche est echap on reprend la valeur initiale et on quitte la boucle 
					if event.key == K_ESCAPE:
						self.menu[pos][0] = self.font.render(pygame.key.name(self.touches[pos_touches]),1,(150,150,150))
						self.touches[pos_touches] = self.touches[pos_touches]
					#Sinon on vient recuperer la touche appuyer et on la modifie puis l'enregistre dans la tableau 1D des touches
					else:
						self.menu[pos][0] = self.font.render(pygame.key.name(event.key),1,(150,150,150))
						self.touches[pos_touches] = event.key
					#On vient transformer la Surface en rectangle
					self.menu[pos][1] = self.menu[pos][0].get_rect()
					#Puis on lui redonne sa position initiale
					self.menu[pos][1].topleft = (self.menu[pos][2] ,self.menu[pos][3])
					return

	#Fonction qui permet de sauvegarder dans le fichier donner a l'initialisation les differentes touches qui serviront pour les menus
	def save(self):
		chaine = []
		#Boucle pour transfomer les integers en string pour venir les enregistrer dans le fichier
		for i in range(len(self.touches)):
			chaine.append(str(self.touches[i]))
		#Transforme une liste en string avec "\n" entre chaque element de la liste
		chaine = "\n".join(chaine)
		#Ouverture du fichier (touches.txt) donner en parametre en mode write 'w' (ecriture en mode texte)
		fichierW = open(self.file,'w')      
		#Ecriture dans le fichier avec la methode write()
		fichierW.write(chaine)
		#Fermeture du fichier avec la methode close()
		fichierW.close()

	#Fonction main qui sera la boucle principale du fichier settings
	def main(self):
		#Boucle infini
		while 1:
			#Gestion des évènements
			for event in pygame.event.get():
				#Recupere la position de la souris
				mouse_pos = pygame.mouse.get_pos()
				#Boucle pour tester la colision des differents menus
				for i in range(len(self.tabText)):
					#Si on quitte le menu on vient sauvegarder les touches dans le fichier et fermer le programme
					if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
						self.save()
						return 'quit'
					#Si l'on vient clic droit sur la valeur a modifier et que l'on est bien dessus
					#On vient afficher la Surface d'attente de la touche puis attendre qu'une touche soit presser
					elif event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0] and self.menu[i][1].collidepoint(mouse_pos) and i%2 == 1:
						self.affichage(i)
						self.modifTouche(i)
					#Si on clic droit sur le bouton retour on vient sauvegarder les touches puis quitter le menu Settings
					elif event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0] and self.retour_rect.collidepoint(mouse_pos):
						self.save()
						return
			#On vient afficher tous les menus sans la partie attente d'une touche
			self.affichage(-1)