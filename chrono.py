import math
from random import *
from pygame.locals import *
import pygame
import pygame.gfxdraw 

class Camembert(object):

    #Parametres : "screen" pour l'ecran de jeu, "bg" la couleur de fond de l'ecran, "fps" pour le nb d'image par seconde
    def __init__(self, screen, bg=(255,255,255), fps=30):
        self.screen = screen
        #Initialisation d'une horloge
        self.clock = pygame.time.Clock()
        self.fps = fps
        self.playtime = 0.0
        self.bg_color = bg
        #Tirage du temps pour la bombe entre 1 et 30 secondes
        self.temps_bombe = self.tirer_temps_aleatoire(1,30)
        self.pause = True

    #Fonction qui vient retourner un entier aleatoire entre 2 valeurs
    #Parametres : "t_min" borne inferieur du tirage, "t_max" borne superieur du tirage
    def tirer_temps_aleatoire(self, t_min,t_max):
        #Retourne un entier entre t_min et t_max compris
        return randint(t_min, t_max)

    #Fonction qui vient dessiner le camembert du temps
    #Parametres : "cx, cy" position sur l'axe x et y du centre du cercle, "r" rayon du cercle , 
    #"angle" angle qui sera effacer du camembert, "color" qui est la couleur du camembert
    def draw_pie(self, cx, cy, r, angle, color):
        #Dessiner un cercle sur une surface (screen), avec une couleur (color), 
        #une position en x et en y, ainsi qu'un rayon
        pygame.draw.circle(self.screen, color, (int(cx), int(cy)), r)
        #Liste des positions des points pour le polygone centre du cercle
        p = [(cx, cy)]
        #Permet de calculer les positions des points sur le cercle en fonction de l'angle
        for n in range(-90, -90+math.floor(angle)):
            #Point en x du point sur cercle via le cosinus
            x = cx + int(1.1*r*math.cos(n*math.pi/180))
            #Point en y du point sur cercle via le sinus
            y = cy+int(1.1*r*math.sin(n*math.pi/180))
            #Enregistrement des positions dans la liste
            p.append((x, y))
        #Enregistrement du centre du cercle pour fermer le polygone
        p.append((cx, cy))
        #Dessine le polygone seulement si on a bien un polygone et non un point
        if len(p) > 2:
            #Dessine le polygone sur l'ecran avec la couleur de fond et les differents points du polygone
            pygame.draw.polygon(self.screen, self.bg_color, p)

    #Fonction qui vient calculer le temps ecouler et l'angle a retirer du camembert
    def run(self):
        #Vient calculer le temps entre 2 appels tick et renvoie un temps en milliseconds
        #fps va limite le nombre d'image par seconde
        milliseconds = self.clock.tick(self.fps)
        #Si le jeu n'est pas en pause on modifie le temps jouer en seconde
        if not self.pause:
            self.playtime += milliseconds / 1000.0
        #Permet de calculer le temps en seconde restant
        temps_restant = self.temps_bombe - self.playtime
        #Permet de calculer le ratio de temps restant
        tx_restant = (temps_restant / self.temps_bombe)
        #Permet de savoir le temps ecoule en angle
        angle = 360 - tx_restant * 360
        #Si le temps restant est superieur a 0 
        if temps_restant > 0:
            #Definit la couleur du camembert en fonction du temps (rouge => blanc)
            bomb_color = (255, max(0,(1-tx_restant)*255), max(0,(1-tx_restant)*255))
            #Appel de la fonction pour dessiner le camembert avec sa position, son rayon, l'angle a retirer et sa couleur
            self.draw_pie(self.screen.get_size()[0]*7/8, self.screen.get_size()[1]*3/4, 80, angle, bomb_color)
            #return pour dire que le temps n'est pas fini
            return False
        else:
            #return pour dire que le temps est fini
            return True

