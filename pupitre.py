import time
#Test d'import RPi.GPIO pour les registres raspberry
#Si fail on creer un fausse classe GPIO pour permettre de continuer a lancer sur ordinateur
try :
    import RPi.GPIO as GPIO
except:
    class FakeGPIO():
        def __init__(self, *params):
            self.BCM = ''
            self.IN = ''
            self.OUT = ''
            self.PUD_UP = ''
            self.LOW = ''
            self.HIGH = ''
        def setup(self, *params):
            pass
        def output(self, *params):
            pass
        def input(self, *params):
            pass
        def setmode(self, *params):
            pass

    GPIO = FakeGPIO()

class Pupitre(object):
    def __init__(self):
        #Function qui vient definir le mode de fonctionnement des broches
        GPIO.setmode(GPIO.BCM)

        #Initialisation des variables pour les 6 pupitres 
        #2 Pins de sorties et 1 d'entrée
        self.P1_IN = 17
        self.P1_OUT1 = 1
        self.P1_OUT2 = 9

        self.P2_IN = 18
        self.P2_OUT1 = 2
        self.P2_OUT2 = 10

        self.P3_IN = 19
        self.P3_OUT1 = 3
        self.P3_OUT2 = 11

        self.P4_IN = 20
        self.P4_OUT1 = 4
        self.P4_OUT2 = 12

        self.P5_IN = 21
        self.P5_OUT1 = 5
        self.P5_OUT2 = 13

        self.P6_IN = 22
        self.P6_OUT1 = 6
        self.P6_OUT2 = 14

        self.tabIn = [self.P1_IN, self.P2_IN, self.P3_IN, self.P4_IN, self.P5_IN, self.P6_IN]
        self.tabOut = [[self.P1_OUT1, self.P2_OUT1, self.P3_OUT1, self.P4_OUT1, self.P5_OUT1, self.P6_OUT1],
                        [self.P1_OUT2, self.P2_OUT2, self.P3_OUT2, self.P4_OUT2, self.P5_OUT2, self.P6_OUT2]]
        self.aimantsStates = []
        self.aimantsStatesOld = []

    #Fonction pour configuration des Pins 
    def GPIO_setup(self):
        for i in range(0,6):
            #Initialisation des Variables en mode entree (aimant)
            GPIO.setup(self.tabIn[i], GPIO.IN, GPIO.PUD_UP)
            #Initialisation des Variables en mode sortie (leds)
            GPIO.setup(self.tabOut[0][i], GPIO.OUT)
            GPIO.setup(self.tabOut[1][i], GPIO.OUT)
            #Affecter une valeur aux sorties (leds eteintes)
            GPIO.output(self.tabOut[0][i], GPIO.LOW)
            GPIO.output(self.tabOut[1][i], GPIO.LOW)
            #creation d'un tableau avec les valeurs des Pins entrees (aimant sur les pupitres)
            self.aimantsStates.append(GPIO.input(self.tabIn[i]))
            self.aimantsStatesOld.append(GPIO.input(self.tabIn[i]))

    #Fonction pour gerer les leds sur les pupitres pour le 6a2 (joueur qui joue et leur nombre de vie) 
    #Parametres : tableau de vie, joueur courant
    def pupitreJoueur(self, tabLife, player = 0):
        for i in range(0,6):
            if i == player:
	            #Affecter une valeur aux sorties (leds allumées)
                GPIO.output(self.tabOut[0][i], GPIO.HIGH)
            else:
	            #Affecter une valeur aux sorties (leds eteintes)
                GPIO.output(self.tabOut[0][i], GPIO.LOW)

            if tabLife[i] == -1:
                GPIO.output(self.tabOut[0][i], GPIO.HIGH)
                GPIO.output(self.tabOut[1][i], GPIO.HIGH)
            elif tabLife[i] == 0:
                GPIO.output(self.tabOut[1][i], GPIO.HIGH)
            else:
                GPIO.output(self.tabOut[1][i], GPIO.LOW)

    #Fonction pour gerer les leds sur les pupitres pour le duel (joueur qui joue et leur nombre de vie)
    #Parametres : tableau de vie, joueur courant
    def pupitreJoueurDuel(self, tabLife, player = 0):
        #definie un tableau pour venir allumé sur le pupitre 1 ou 3
        pupi = [1, 3]
        #Gestion du joueur courant (J1 ou J2)
        if player == 0:
            GPIO.output(self.tabOut[0][2], GPIO.HIGH)
            GPIO.output(self.tabOut[1][2], GPIO.LOW)
        else:
            GPIO.output(self.tabOut[0][2], GPIO.LOW)
            GPIO.output(self.tabOut[1][2], GPIO.HIGH)

        #Gestion des vies des 2 joueurs sur les pupitres 1 et 3 
        for i in range(0,2):
            if tabLife[i] == -1:
                GPIO.output(self.tabOut[0][pupi[i]], GPIO.HIGH)
                GPIO.output(self.tabOut[1][pupi[i]], GPIO.HIGH)
            elif tabLife[i] == 0:
                GPIO.output(self.tabOut[0][pupi[i]], GPIO.LOW)
                GPIO.output(self.tabOut[1][pupi[i]], GPIO.HIGH)
            elif tabLife[i] == 1:
                GPIO.output(self.tabOut[0][pupi[i]], GPIO.HIGH)
                GPIO.output(self.tabOut[1][pupi[i]], GPIO.LOW)
            else:
                GPIO.output(self.tabOut[0][pupi[i]], GPIO.LOW)
                GPIO.output(self.tabOut[1][pupi[i]], GPIO.LOW)

    #Fonction qui permet de faire une animation avec les lumieres en cas de vie perdu ou de partie finie
    #Parametre: temps entre le changement d'etat des Leds
    def loseLumiere(self, temps):
        for _ in range(0,5):
            for i in range(0,6):
                GPIO.output(self.tabOut[0][i], GPIO.HIGH)
                GPIO.output(self.tabOut[1][i], GPIO.LOW)
            time.sleep(temps)
            for i in range(0,6):
                GPIO.output(self.tabOut[0][i], GPIO.LOW)
                GPIO.output(self.tabOut[1][i], GPIO.HIGH)
            time.sleep(temps)

    #Fonction qui vient tester s'il y a un changement d'etat de l'aimant par rapport au joueur en cours
    #Parametre: defini le joueur qui joue pour verifier s'il fait l'action de passer au joueur suivant 
    def actionChange(self, player):
        self.aimantsStates[player] = GPIO.input(self.tabIn[player])
        if self.aimantsStates[player] != self.aimantsStatesOld[player]:
            self.aimantsStatesOld[player] = self.aimantsStates[player]
            return True
        return False