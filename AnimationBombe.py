import pygame
from pygame.locals import *

class AnimationBombe(object):
    	
	def __init__(self, screen):
		self.angleDepart = 0
		self.angle = 0
		self.sensRotation = False
		self.image_orig = pygame.image.load('Images/bombe.png').convert_alpha()
		self.image = self.image_orig.copy()
		self.screen = screen
		self.image_rect = self.image_orig.get_rect(center=(self.screen.get_size()[0]/2,self.screen.get_size()[1]/2))
		self.pause = True
		self.clock = pygame.time.Clock()

	def setAngle(self, angle):
		if self.sensRotation:
			self.angle = -angle
		else:
			self.angle = angle

	def inverse(self):
		if self.pause == False:
			self.angle *= -1
			self.sensRotation = not self.sensRotation

	def run(self):
		#Rotation de l'image 
		self.image_rect = self.image.get_rect(center=(self.screen.get_size()[0]/2,self.screen.get_size()[1]/2))
		val_return = False
		if self.pause == False:
    		#Modification de l'image par seconde et de la rotation de l'image
			self.clock.tick(10) 
			self.angleDepart += self.angle
			if self.angleDepart == (180+self.angle) or self.angleDepart ==(-180+self.angle):
				self.reset()
				self.sensRotation = not self.sensRotation
				val_return = True
			self.image = pygame.transform.rotate(self.image_orig, self.angleDepart)
			self.image_rect = self.image.get_rect(center=(self.screen.get_size()[0]/2,self.screen.get_size()[1]/2))
			return val_return

	def reset(self):
		print ("bouum !")
		self.angle = 0
		self.angleDepart = 0
		self.sensRotation = not self.sensRotation
		self.pause = True
		self.image = pygame.transform.rotate(self.image_orig, self.angleDepart)
		self.image_rect = self.image.get_rect(center=(self.screen.get_size()[0]/2,self.screen.get_size()[1]/2))
 