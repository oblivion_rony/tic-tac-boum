import game
import chrono
import pygame
import Son
import pupitre
import ListingPhoneme
from pygame.locals import *


class SixADeux():
    #Parametres : "screen" pour l'ecran de jeu, "color" pour la couleur de fond, "settings" objet de la classe pour avoir un lien avec les touches définies
    #"player_name" pour avoir les differents nom des joueurs, "nb_life" pour le nombre de vie au debut du mode 6 a 2
    def __init__(self, screen, color, settings, player_name = ['J1','J2','J3','J4','J5','J6'], nb_life=1):
        self.continuer = False
        #Instance de la classe SixADeux avec "sixADeux" en nom, le passage du tableau des joueurs, ainsi que le nombre de vie au debut
        self.sixADeux = game.SixADeux('sixADeux',player_name,nb_life)
        self.screen = screen
        self.color = color
        self.pupitre = pupitre.Pupitre()
        #Instance de la classe Cammebert avec la surface ou dessiner ainsi que la couleur du fond
        self.pie_chrono = chrono.Camembert(screen, self.color)
        self.son = Son.Son()
        self.settings = settings
        self.listingPhoneme = ListingPhoneme.Phoneme(screen)
        #Lance la fonction pour configurer les pins du GPIO
        self.pupitre.GPIO_setup()
        
    #Fonction lose qui vient gerer les sons, les lumieres ainsi que la gestion du reset quand une personne perd une vie
    #lier a la perte de vie d'un des joueurs
    def lose(self, player):
        #Met le son "tictac" en pause
        self.son.pauseSon(self.son.bruitages_name.index("tictac"))
        #Joue le son "explosion"
        self.son.playSon(self.son.bruitages_name.index("explosion"))
        #Appel de la fonction qui vient retirer une vie avec la position du joueur daans le tableau
        #Renvoie "true" si le mode 6 a 2 est fini, "None" si un joueur est elimine et "False" si un joueur a perdu une vie 
        val = self.sixADeux.wrong(player)
        #Test la valeur de retour 
        if val:
            #Joue le son "Tic_Tac_victoire"
            self.son.playSon(self.son.bruitages_name.index("Tic_Tac_victoire"))
            #Reset la partie pour pouvoir y rejouer plus tard
            self.sixADeux.reset()
            #Lance le jeu de lumiere de la defaite avec un temps avec chaque changement
            self.pupitre.loseLumiere(0.1)
            #Met la variable a False pour quitter le fichier SixADeux
            self.continuer = False
        elif val == None:
            #Lance le jeu de lumiere de la defaite avec un temps avec chaque changement
            self.pupitre.loseLumiere(0.1)
        else:
            #Lance le jeu de lumiere de la defaite avec un temps avec chaque changement
            self.pupitre.loseLumiere(0.2)
            #Passe au joueur suivant
            self.sixADeux.next()

        #Inverse la valeur de pause du sixADeux True => False | False => True
        self.sixADeux.pause = not self.sixADeux.pause
        #Inverse la valeur de pause du chrono True => False | False => True
        self.pie_chrono.pause = not self.pie_chrono.pause
        #Retire un nombre aleatoire pour la manche suivante
        self.pie_chrono.temps_bombe = self.pie_chrono.tirer_temps_aleatoire(1,30)
        #Remet le temps de jeu a 0
        self.pie_chrono.playtime = 0.0
        #Retire un nouveau phoneme pour la manche suivante
        self.listingPhoneme.tirer_phoneme()

    #Fonction principale du fichier qui va s'occuper du mode 6 a 2
    def start(self):
        #Lance le jingle "6a2" pour une explication des regles du mode 6 a 2
        #self.son.playVideo(self.son.jingles_name.index("6a2"))
        #Variable de la boucle infini
        self.continuer = True
        #Tire un phoneme pour la premiere manche
        self.listingPhoneme.tirer_phoneme()
        #Boucle infinie
        while self.continuer:
            #Appel de la Fonction qui va s'occupe des lumieres du mode 6 a 2
            self.pupitre.pupitreJoueur(self.sixADeux.lifes, self.sixADeux.num_joueur_actif_total)
            #Appel de la Fonction qui test si le joueur courant a fait l'action de passer a sa gauche et renvoie True si c'est le cas
            if self.pupitre.actionChange(self.sixADeux.num_joueur_actif_total):
                #Appel de la fonction pour passer au joueur suivant
                self.sixADeux.next()
                #Joue le son "6a2_passe_bombe"
                self.son.playSon(self.son.bruitages_name.index("6a2_passe_bombe"))

            # Gestion des évènements
            for event in pygame.event.get():
			    #Si un de ces événements est de type QUIT ou que l'on appui sur echap 
                if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:		
                    #On arrete la boucle
                    self.continuer = False
                    #On renvoie la valeur "quit" pour fermer le programme
                    return 'quit'
		        #Si un evenement est de type touche appuyer
                elif event.type == KEYDOWN:
				    #Si on appuie sur la touche pour faire perdre le joueur avant
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    if event.key == self.settings.touches[4]:
                        #Appel de la Fonction lose avec le nom du joueur qui a jouer dans le tableau temporaire
                        #pour le avoir l'index dans le tableau des joueurs
                        self.lose(self.sixADeux.players.index(self.sixADeux.players_tmp[self.sixADeux.num_joueur_actif-1]))
				    #Si on appuie sur la touche pour faire perdre le joueur 1 (position 0)
                    #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[5]:
                        self.lose(0)
				    #Si on appuie sur la touche pour faire perdre le joueur 2 (position 1)
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[6]:
                        self.lose(1)
				    #Si on appuie sur la touche pour faire perdre le joueur 3 (position 2)
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[7]:
                        self.lose(2)
				    #Si on appuie sur la touche pour faire perdre le joueur 4 (position 3)
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[8]:
                        self.lose(3)
				    #Si on appuie sur la touche pour faire perdre le joueur 5 (position 4)
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[9]:
                        self.lose(4)
				    #Si on appuie sur la touche pour faire perdre le joueur 6 (position 5)
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[10]:
                        self.lose(5)
				    #Si on appuie sur la touche pour passer au joueur suivant
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[11]:
                        #On appelle la methode next pour passer au joueur suivant
                        self.sixADeux.next()
                        #Joue le son "6a2_passe_bombe"
                        self.son.playSon(self.son.bruitages_name.index("6a2_passe_bombe"))
				    #Si on appuie sur la touche pour revenir en arriere par rapport a l'action precedente
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[12]:
                        #On appelle la methode back avec la position dans le tableau -1
                        self.sixADeux.back(self.sixADeux.position-1)
				    #Si on appuie sur la touche pour mettre pause
                    #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[13]:
	                    #Inverse la valeur de pause du sixADeux True => False | False => True
                        self.sixADeux.pause = not self.sixADeux.pause
                        #Inverse la valeur de pause du chrono True => False | False => True
                        self.pie_chrono.pause = not self.pie_chrono.pause
                        #Si le jeu est en pause
                        if self.sixADeux.pause:
                            #On met en pause le son "tictac"
                            self.son.pauseSon(self.son.bruitages_name.index("tictac"))
                        else:
                            #Sinon on joue le son "tictac"
                            self.son.playSon(self.son.bruitages_name.index("tictac"))
				    #Si on appuie sur la touche pour lancer le jingle
				    #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[3]:
                        #On met la valeur de pause du sixADeux a True
                        self.sixADeux.pause = True
                        #On met la valeur de pause du chrono a True
                        self.pie_chrono.pause = True
                        #Relance le jingle "6a2" pour reexpliquer les regles du mode 6 a 2
                        self.son.playVideo(self.son.jingles_name.index("6a2"))
                    #Si on appuie sur la touche pour augmenter le son des bruitages
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[14]:
                        self.son.volumeM()
                    #Si on appuie sur la touche pour diminuer le son des bruitages
			        #Touche definie dans le fichier touches.txt et mis sous tableau dans settings
                    elif event.key == self.settings.touches[15]:
                        self.son.volumePP()


            #Remplissage d'une couleur de fond
	        #Parametre: couleur (r,g,b)
            self.screen.fill(self.color)
            #Appel de la fonction pour afficher le phoneme
            self.listingPhoneme.afficherPhon()
            #Test si le timer (camembert) est fini
            if self.pie_chrono.run():
                #Lance la Fonction lose avec l'indice du joueur actif du tableau des joueurs
                self.lose(self.sixADeux.num_joueur_actif_total)
            # Rafraîchissement de l'écran
            pygame.display.flip()