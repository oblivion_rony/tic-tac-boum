import pygame
from pygame.locals import *

class Game:
	#Fonction __init__ apppeler lors de la création d'un objet qui permet l'initialisation des variables 
	#Parametres : "name" nom du mode de jeu, "players" tableau avec le nom des joueurs, "nb_life" nombre vie que les joueurs commencent
	def __init__(self, name, players, nb_life):
		self.name = name
		self.players = players
		#Initialise un tableau avec le meme nombre de vie pour chaque joueur
		self.lifes = [nb_life] * len(players)
		#Copie des listes dans d'autres listes
		self.players_tmp = self.players[:]
		self.lifes_tmp = self.lifes[:]
		#Index du joueur qui joue encore en vie
		self.num_joueur_actif = 0
		#Index du joueur qui joue dans le tableau globale
		self.num_joueur_actif_total = 0
		#Tableau qui permet de remettre les valeurs de base
		self.tabReset = [self.name,self.players[:],self.lifes[:]]
		#Met le jeu en pause
		self.pause = True
		#Tableau qui enregistre chaque modification de vie 
		self.history = [self.lifes[:]]
		#Position dans l'historique pour quand on le modifie ou on revient en arriere
		self.position = 0

	def reset(self):
		self.name = self.tabReset[0]
		self.players = self.tabReset[1][:]
		self.lifes = self.tabReset[2][:]
		#copie des listes dans d'autres listes
		self.players_tmp = self.tabReset[1][:]
		self.lifes_tmp = self.tabReset[2][:]
		#Index du joueur qui joue encore en vie
		self.num_joueur_actif = 0
		#Index du joueur qui joue dans le tableau globale
		self.num_joueur_actif_total = 0
		#On remet le tableau qui enregistre chaque modification de vie en valeur initiale
		self.history = [self.lifes[:]]
		#Position dans l'historique pour quand on le modifie ou on revient en arriere
		self.position = 0
		

	#Permet de revenir en arriere dans l'historique
	#Parametre : "pos" position dans l'historique a choisir pour le retour arriere
	def back(self, pos):
		#Remet les vies des joueurs dans le tableau vie en fonction de l'historique
		self.lifes = self.history[pos][:]
		#Remet les vies des joueurs dans le tableau temporaire de vie en fonction de l'historique
		self.lifes_tmp = self.history[pos][:]
		#Ajoute dans l'historique le changement des vies
		self.history.append(self.lifes[:])
		print(self.history)
		#Modifie la position dans l'historique pour revenir plusieurs fois en arriere
		self.position = pos

    	
	#Permet de passer au joueur suivant
	def next(self):
		#Test si le jeu est en pause
		if not self.pause:
			#Copie des listes dans d'autres listes
			self.lifes_tmp = self.lifes[:]
			self.players_tmp = self.players[:]

			print(self.lifes_tmp)
			print(self.players_tmp)

			#On vient compter le nombre d'occurence de -1
			counter_item = self.lifes_tmp.count(-1)
			#On boucle sur le nombre d'occurence
			for _ in range(counter_item):
				#On recupere l'index de la premiere occurence de -1
				val = self.lifes_tmp.index(-1)
				#On supprime les elements des listes temporaires via l'index
				del self.lifes_tmp[val]
				del self.players_tmp[val]

			print(self.players_tmp)
			
			#On test si on est le dernier joueur et donc changer le prochain joueur au premier
			if self.num_joueur_actif+1 >= len(self.lifes_tmp):
				#On met le prochain joueur au premier de la liste des joueurs restants
				self.num_joueur_actif = 0
				#On recupere le nom du joueur qui va jouer via l'index pour avoir son index dans la tableau global
				self.num_joueur_actif_total = self.players.index(self.players_tmp[self.num_joueur_actif])
			else:
				#On incremente l'index du joueur de la liste des joueurs restants
				self.num_joueur_actif +=1
				#On recupere le nom du joueur qui va jouer via l'index pour avoir son index dans la tableau global
				self.num_joueur_actif_total = self.players.index(self.players_tmp[self.num_joueur_actif])

			print('joueur {} a joué'.format(self.players_tmp[self.num_joueur_actif-1]))
			print('joueur {} joue'.format(self.players_tmp[self.num_joueur_actif]))

#Class avec un heritage de la class Game
class Duel(Game):
    #Fonction __init__ apppeler lors de la création d'un objet qui permet l'initialisation des variables 
	#Parametres : "name" nom du mode de jeu, "players" tableau avec le nom des joueurs, "nb_life" nombre vie que les joueurs commencent
	def __init__(self, name, players, nb_life):
    	#Permet d'appeler le constructeur __init__ de la class mere
		super().__init__(name, players, nb_life)				
		
	#Permet de diminuer le nb de vie d'un joueur
	#Parametre : "player" indice du joueur qui a perdu une vie
	def wrong(self, player):
		if not self.pause:
			#On verifie si les 2 listes sont differentes
			if self.lifes != self.lifes_tmp:
				#Si c'est le cas on met a jour la liste temporaire
				self.lifes_tmp = self.lifes[:]

			#Si le joueur a 0 vie il perd une vie et donc la partie
			if self.lifes[player] == 0:
				self.lifes[player] -= 1
				print("le joueur {} est éliminé.".format(self.players[player]))
				#On test quel est le joueur qui est elimine
				if player == 0:
					print("le joueur {} a gagné.".format(self.players[player+1]))
				else:
					print("le joueur {} a gagné.".format(self.players[player-1]))
				return True
			#Sinon on diminue de 1 la vie du joueur			
			else:
				self.lifes[player] -= 1
				print("le joueur {0} lui reste {1} vie(s).".format(self.players[player],self.lifes[player]))
				return False

#Class avec un heritage de la class Game
class SixADeux(Game):
    #Fonction __init__ apppeler lors de la création d'un objet qui permet l'initialisation des variables
	#Parametres : "name" nom du mode de jeu, "players" tableau avec le nom des joueurs, "nb_life" nombre vie que les joueurs commencent
	def __init__(self, name, players, nb_life):
    	#Permet d'appeler le constructeur __init__ de la class mere
		super().__init__(name, players, nb_life)				

	#Permet de diminuer le nb de vie d'un joueur
	#Parametre : "player" indice du joueur qui a perdu une vie
	def wrong(self, player):
		if not self.pause:
			#On verifie si les 2 listes sont les memes
			if self.lifes != self.lifes_tmp:
				self.lifes_tmp = self.lifes[:]

			#Si un joueur est a 0 et qui se trompe il est elimine
			if self.lifes[player] == 0:
				self.lifes[player] -= 1
				#Met a jour la position de l'historique a la fin
				self.position = len(self.history)
				#Ajout la derniere modification des vies dans l'historique
				self.history.append(self.lifes[:])
				print(self.history)
				print("le joueur {} est éliminé.".format(self.players[player]))
				#On vient compter le nombre d'occurence de -1
				counter_item = self.lifes.count(-1)
				#Si on a 4 joueurs avec -1 (elimines) la manche est finie
				if counter_item == 4:
					print('La manche est finie')
					#On renvoie la valeur True
					return True
				#On remet toutes les valeurs de vie dans le tableau
				self.lifes_tmp = self.lifes[:]
				#On remet toutes les valeurs des joueurs dans le tableau
				self.players_tmp = self.players[:]
				#On boucle sur le nombre d'occurence
				for _ in range(counter_item):
					#On recupere l'index de la premiere occurence de -1
					val = self.lifes_tmp.index(-1)
					#On supprime les elements des listes temporaires via l'index
					del self.lifes_tmp[val]
					del self.players_tmp[val]
				#On test le joueur actif pour savoir si c'est le dernier de la liste
				if self.num_joueur_actif == len(self.players_tmp):
					#On met le joueur actif au premier
					self.num_joueur_actif = 0
					#On recupere le nom du joueur qui va jouer via l'index pour avoir son index dans la tableau global
					self.num_joueur_actif_total = self.players.index(self.players_tmp[self.num_joueur_actif])
				else:
					#On recupere le nom du joueur qui va jouer via l'index pour avoir son index dans la tableau global
					self.num_joueur_actif_total = self.players.index(self.players_tmp[self.num_joueur_actif])
				return None
			#Si un joueur est deja elimine on suppose une faute de frappe
			elif self.lifes[player] == -1:
				print("déjà éliminé, joueur : {}".format(player))
			#Sinon on diminue de 1 la vie du joueur			
			else:
				#On diminue le nombre de vie du joueur 
				self.lifes[player] -= 1
				print("le joueur {0} lui reste {1} vie(s).".format(self.players[player],self.lifes[player]))
				#On met la position au dernier element de l'historique
				self.position = len(self.history)
				#On ajoute la modification de vie dans l'historique
				self.history.append(self.lifes[:])
				print(self.history)
			return False