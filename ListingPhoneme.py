import pygame
from random import *
from pygame.locals import *
import glob

class Phoneme(object):

	#Parametre : "img_dir" dossier ou sont enregistrer les phonemes
	def __init__(self,screen, img_dir = "Images/Phonemes"):
		#glob vient lire les elements dans un dossier 
		self.phoneme = glob.glob("{}/*.png".format(img_dir))
		#Creation d'une copie de la liste
		self.phoneme_tmp = self.phoneme[:]
		self.phoneme_current = None
		self.screen = screen

	#Fonction pour tirer un phoneme sans remise
	def tirer_phoneme(self):
		#Tire un nombre aleatoire entre 0 et la taille-1 de la liste de phoneme
		i=randint(0, len(self.phoneme_tmp)-1)
		#Recupere l'element de la liste a la position choisit et le retire de la liste
		self.phoneme_current = self.phoneme_tmp.pop(i)
		#Si taille est egale a 0 on renitialise la liste 
		if len(self.phoneme_tmp) == 0:
			self.reset()

	#Fonction reset pour renitialiser la liste
	def reset(self):
		self.phoneme_tmp = self.phoneme[:]

	#Fonction qui vient afficher le phoneme tirer au centre de l'ecran
	def afficherPhon(self):
		if self.phoneme_current != None:
			image = pygame.image.load(self.phoneme_current).convert_alpha()
			image_rect = image.get_rect()

			#Permet de definir la position du titre en fonction de la taille de l'ecran
			imagex = self.screen.get_size()[0]/2
			imagey = self.screen.get_size()[1]/2
			#Permet de definir le centre du rectangle avec les positions
			image_rect.center = (imagex, imagey)

			self.screen.blit(image, image_rect)